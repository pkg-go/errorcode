package errorcode

import (
	"errors"
	"fmt"
)

type ErrorCode int

type errorWithCode struct {
	code    ErrorCode
	message string
	origin  error
}

func Wrap(err error, code ErrorCode, message string) error {
	return &errorWithCode{
		code:    code,
		message: message,
		origin:  err,
	}
}

func New(code ErrorCode, message string) error {
	return Wrap(nil, code, message)
}

func Code(err error) (ErrorCode, bool) {
	if err, ok := err.(interface {
		Code() ErrorCode
	}); ok {
		return err.Code(), true
	}
	return 0, false
}

func Is(err error, code ErrorCode) bool {
	if err == nil {
		return false
	}
	if c, ok := Code(err); ok && c == code {
		return true
	}
	e := errors.Unwrap(err)
	return Is(e, code)
}

func (e *errorWithCode) Code() ErrorCode {
	return e.code
}

func (e *errorWithCode) Error() string {
	str := fmt.Sprintf("[%d] %s", e.code, e.message)
	if e.origin != nil {
		str = fmt.Sprintf("%s: %v", str, e.origin)
	}
	return str
}

func (e *errorWithCode) Unwrap() error {
	return e.origin
}

func (e *errorWithCode) Is(target error) bool {
	if code, ok := Code(target); ok {
		return e.code == code
	}
	return false
}
