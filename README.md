# errorcode

`errorcode` is a package for Go that provides a mechanism for representing errors using `error codes`, wrapping the original error to allow error chaining.


## Usage

```go
package main

import (
	"errors"
	"fmt"
	"log"

	"gitlab.com/pkg-go/errorcode"
)

const (
	ErrCodeNotFound errorcode.ErrorCode = iota
	ErrCodeUnknown
)

func GetValues() ([]string, error) {
	return nil, errors.New("random error")
	// return []string{"one", "two"}, nil
}

func FindValue(value string) (int, error) {
	values, err := GetValues()
	if err != nil {
		return -1, errorcode.Wrap(err, ErrCodeUnknown, "main.GetValues")
	}
	for i, v := range values {
		if v == value {
			return i, nil
		}
	}
	return -1, errorcode.New(ErrCodeNotFound, "main.FindValue")
}

func main() {
	_, err := FindValue("three")

	if errorcode.Is(err, ErrCodeNotFound) {
		fmt.Println("not found")
	}

	if errorcode.Is(err, ErrCodeUnknown) {
		log.Fatal(err)
	}
}
```