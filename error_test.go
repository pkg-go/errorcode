package errorcode

import (
	"errors"
	"testing"
)

const (
	errCode1 ErrorCode = iota + 1
	errCode2
	errCode3
	errCode4
)

func TestNew(t *testing.T) {
	t.Parallel()

	err1 := New(errCode1, "error 1")
	if err1 == nil {
		t.Error("New() should return error")
	}
	if err1.Error() != "[1] error 1" {
		t.Error("New() should return error with message 'error 1'")
	}
}

func TestCode(t *testing.T) {
	t.Parallel()

	err := errors.New("error")
	err1 := New(errCode1, "error 1")
	err2 := Wrap(err1, errCode2, "error 2")

	tests := []struct {
		name string
		err  error
		code ErrorCode
		ok   bool
	}{
		{
			name: "Code(err1) should return 1",
			err:  err1,
			code: errCode1,
			ok:   true,
		}, {
			name: "Code(err2) should return 2",
			err:  err2,
			code: errCode2,
			ok:   true,
		}, {

			name: "Code(err) should not return a code",
			err:  err,
			ok:   false,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			code, ok := Code(test.err)
			if ok != test.ok {
				t.Errorf("Code(%v) return an unexpected code", test.err)
			} else if ok && code != test.code {
				t.Errorf("Code(%v) should return %v", test.err, test.code)
			}
		})
	}

}

func TestWrap(t *testing.T) {
	t.Parallel()

	origin := errors.New("origin error")
	err1 := Wrap(origin, errCode1, "error 1")
	if err1 == nil {
		t.Error("Wrap() should return error")
	}
	if err := errors.Unwrap(err1); err != origin {
		t.Error("Wrap() should return error with origin error")
	}
}

func TestUnwrap(t *testing.T) {
	t.Parallel()

	err1 := New(errCode1, "error 1")
	err2 := Wrap(err1, errCode2, "error 2")
	err3 := Wrap(err2, errCode3, "error 3")

	if err := errors.Unwrap(err3); err != err2 {
		t.Error("Unwrap(err3) should return error with code 2")
	}
	if err := errors.Unwrap(err2); err != err1 {
		t.Error("Unwrap(err2) should return error with code 1")
	}
	if err := errors.Unwrap(err1); err != nil {
		t.Error("Unwrap(err1) should return nil")
	}
}

func TestIs(t *testing.T) {
	t.Parallel()

	err1 := New(errCode1, "error 1")
	err2 := Wrap(err1, errCode2, "error 2")
	err3 := New(errCode3, "error 3")

	tests := []struct {
		name string
		err  error
		code ErrorCode
		ok   bool
	}{
		{
			name: "Is(err1) should return true for error 1",
			err:  err1,
			code: errCode1,
			ok:   true,
		}, {
			name: "Is(err2) should return true for error 2",
			err:  err2,
			code: errCode2,
			ok:   true,
		}, {
			name: "Is(err3) should return true for error 3",
			err:  err3,
			code: errCode3,
			ok:   true,
		}, {
			name: "Is(err2) should return true for error 1",
			err:  err2,
			code: errCode1,
			ok:   true,
		}, {
			name: "Is(err1) should return false for error 2",
			err:  err1,
			code: errCode2,
			ok:   false,
		}, {
			name: "Is(err2) should return false for error 3",
			err:  err2,
			code: errCode3,
			ok:   false,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			if ok := Is(test.err, test.code); ok != test.ok {
				t.Errorf("Is(%v, %v) should return %v", test.err, test.code, test.ok)
			}
		})
	}
}

func TestErrorsIs(t *testing.T) {
	t.Parallel()

	err1 := New(errCode1, "error 1")
	err2 := Wrap(err1, errCode2, "error 2")
	err3 := Wrap(err2, errCode3, "error 3")
	err4 := New(errCode4, "error 4")

	tests := []struct {
		name   string
		err    error
		target error
		ok     bool
	}{
		{
			name:   "errors.Is(err2, err1) should return true",
			err:    err2,
			target: err1,
			ok:     true,
		}, {
			name:   "errors.Is(err3, err1) should return true",
			err:    err3,
			target: err1,
			ok:     true,
		}, {
			name:   "errors.Is(err4, err4) should return true",
			err:    err4,
			target: err4,
			ok:     true,
		}, {
			name:   "errors.Is(err4, err1) should return false",
			err:    err4,
			target: err1,
			ok:     false,
		}, {
			name:   "errors.Is(err1, err3) should return false",
			err:    err1,
			target: err3,
			ok:     false,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			if ok := errors.Is(test.err, test.target); ok != test.ok {
				t.Errorf("errors.Is(%v, %v) should return %v", test.err, test.target, test.ok)
			}
		})
	}
}
